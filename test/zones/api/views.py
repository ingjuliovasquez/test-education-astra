from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from pprint import pprint

from zones.models import Zone, Distribution
from zones.api.serializers import ZoneSerializer



@api_view(['POST'])
def edit(request):
    print(request.data)
    zone_id = request.data.get('id')
    name = request.data.get('name')

    zoneEntity = Zone.objects.filter(pk=zone_id).first()
    zoneValidName = Zone.objects.filter(name=name).values()
    print(zoneValidName.count())
    zoneValidName = Zone.objects.filter(name=name).values()
    print(zoneValidName.count())
    if not zoneEntity:
        return Response('', status=status.HTTP_400_BAD_REQUEST)
    if(zoneValidName.count() > 1):
        return Response('Name already taken', status=status.HTTP_400_BAD_REQUEST)
    elif zoneValidName.count() > 0:
        print(zoneValidName.first().name)
        print(name)
        zoneEntity.name = name
        if zoneValidName.first().name != name:
            zoneEntity.save()
    
    distributions = request.data.get('distributions')##array of objects
    for distribution in distributions:
        entityDistribution = Distribution.objects.filter(pk=distribution.get('id')).first()
        try:
            intId = int(distribution.get('percentage'))
        except:
            return Response('percentage should be integer, not string', status=status.HTTP_400_BAD_REQUEST) 
        try:
            intId = int(distribution.get('percentage'))
        except:
            return Response('percentage should be integer, not string', status=status.HTTP_400_BAD_REQUEST) 
        if(entityDistribution):
            entityDistribution.percentage = distribution.get('percentage')
            entityDistribution.save()
            # print(entityDistribution)
        else:
            newDistribution = Distribution(percentage = distribution.get('percentage'), zone = zoneEntity)
            newDistribution.save()
            print(newDistribution)
            
    return Response()

@api_view(['DELETE'])
def delete(request, id = "id"):
    distribution = Distribution.objects.filter(pk=id).first()
    print(distribution)
    distribution.delete()
    return Response("deleted", 200)

@api_view(['GET'])
def list(request):
    zones = zones_data = ZoneSerializer(Zone.objects.all(), many=True).data
    print(zones)
    return Response(zones, 200)
